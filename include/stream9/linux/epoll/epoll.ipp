#ifndef STREAM9_LINUX_EPOLL_EPOLL_IPP
#define STREAM9_LINUX_EPOLL_EPOLL_IPP

#include "namespace.hpp"

#include <stream9/linux/epoll/epoll.hpp>

#include <sys/epoll.h>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/to_string.hpp>

namespace stream9::linux {

namespace epoll_ {

    inline void
    control(int const epfd,
            int const op,
            int const fd,
            struct ::epoll_event* const ev)
    {
        if (::epoll_ctl(epfd, op, fd, ev) == -1) {
            throw_error(
                "epoll_ctl()",
                linux::make_error_code(errno), {
                    { "epfd", epfd },
                    { "op", epoll_op_to_symbol(op) },
                    { "fd", fd },
                    { "event", str::to_string(ev) },
                });
        }
    }

} // namespace epoll_

template<std::size_t N>
epoll<N>::
epoll(int const flags/*= 0*/)
    : m_fd { ::epoll_create1(flags) }
{
    if (m_fd == -1) {
        throw_error("epoll_create1()",
            linux::make_error_code(errno), {
                { "flags", flags },
            });
    }
}

template<std::size_t N>
void epoll<N>::
add(fd_ref const fd, struct ::epoll_event const& ev)
{
    try {
        epoll_::control(
            m_fd, EPOLL_CTL_ADD, fd, const_cast<struct ::epoll_event*>(&ev));
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "fd", fd },
            { "ev", ev },
        });
    }
}

template<std::size_t N>
void epoll<N>::
add(fd_ref const fd, std::uint32_t events)
{
    try {
        add(fd, {
            .events = events,
            .data = { .fd = fd },
        });
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "fd", fd },
            { "events", events },
        });
    }
}

template<std::size_t N>
void epoll<N>::
modify(fd_ref const fd, struct ::epoll_event const& ev)
{
    try {
        epoll_::control(
            m_fd, EPOLL_CTL_MOD, fd, const_cast<struct ::epoll_event*>(&ev));
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "fd", fd },
            { "ev", ev },
        });
    }
}

template<std::size_t N>
void epoll<N>::
modify(fd_ref const fd, std::uint32_t events)
{
    try {
        modify(fd, {
            .events = events,
            .data = { .fd = fd },
        });
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "fd", fd },
            { "events", events },
        });
    }
}

template<std::size_t N>
void epoll<N>::
remove(fd_ref const fd)
{
    try {
        epoll_::control(m_fd, EPOLL_CTL_DEL, fd, nullptr);
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "fd", fd },
        });
    }
}

template<std::size_t N>
epoll<N>::epoll_event_list epoll<N>::
wait(duration const timeout/*= -1*/)
{
    try {
        st9::safe_integer const rc =
              ::epoll_wait(m_fd, m_buf, N, timeout.count());
        if (rc == -1) {
            if (errno == EINTR) return { m_buf, 0 };
            else {
                throw_error(
                    "epoll_wait()",
                    linux::make_error_code(errno) );
            }
        }

        return { m_buf, rc };
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "timeout", str::to_string(timeout) }
        });
    }
}

template<std::size_t N>
epoll<N>::epoll_event_list epoll<N>::
pwait(sigset_t const& sigmask,
      duration const timeout/*= -1*/)
{
    try {
        st9::safe_integer const count =
            ::epoll_pwait(m_fd, m_buf, N, timeout.count());
        if (count == -1) {
            if (errno == EINTR) return { m_buf, 0 };
            else {
                throw_error(
                    "epoll_pwait()",
                    linux::make_error_code(errno)
                );
            }
        }

        return { m_buf, count };
    }
    catch (...) {
        rethrow_error({
            { "epfd", m_fd },
            { "timeout", str::to_string(timeout) }
        });
    }
}

} // namespace stream9::linux

namespace stream9::json {

inline void
tag_invoke(value_from_tag, value& v, ::epoll_event const& ev)
{
    auto& o = v.emplace_object();

    o["events"] = stream9::linux::epoll_events_to_symbol(ev.events);
    o["data"] = ev.data.u64;
}

} // namespace stream9::json

#endif // STREAM9_LINUX_EPOLL_EPOLL_IPP
