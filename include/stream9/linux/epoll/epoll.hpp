#ifndef STREAM9_LINUX_EPOLL_EPOLL_HPP
#define STREAM9_LINUX_EPOLL_EPOLL_HPP

#include <chrono>
#include <cstdint>
#include <span>
#include <string>
#include <string_view>

#include <sys/epoll.h>

#include <stream9/linux/fd.hpp>

namespace stream9::linux {

template<std::size_t MaxEvents = 16>
class epoll
{
public:
    using epoll_event_list = std::span<struct ::epoll_event>;
    using duration = std::chrono::duration<int, std::milli>;

public:
    // essential
    epoll(int flags = 0);

    // query
    fd_ref fd() const noexcept { return m_fd; }

    // modifier
    void add(fd_ref fd, struct ::epoll_event const& ev);
    void add(fd_ref fd, std::uint32_t events); // epoll_event.data.fd = fd

    void modify(fd_ref fd, struct ::epoll_event const& ev);
    void modify(fd_ref fd, std::uint32_t events); // epoll_event.data.fd = fd

    void remove(fd_ref fd);

    // command
    epoll_event_list
        wait(duration timeout = duration(-1));

    epoll_event_list
        pwait(::sigset_t const& sigmask, duration timeout = duration(-1));

private:
    linux::fd m_fd;
    ::epoll_event m_buf[MaxEvents];
};

std::string_view
epoll_op_to_symbol(int op) noexcept;

std::string
epoll_events_to_symbol(std::uint32_t events);

} // namespace stream9::linux

namespace stream9::json {

struct value_from_tag;
class value;

void tag_invoke(value_from_tag, value&, ::epoll_event const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_EPOLL_EPOLL_HPP
