#ifndef STREAM9_LINUX_EPOLL_NAMESPACE_HPP
#define STREAM9_LINUX_EPOLL_NAMESPACE_HPP

namespace stream9::strings {}

namespace stream9::linux {

namespace st9 { using namespace stream9; }
namespace str { using namespace stream9::strings; }

} // namespace stream9::linux

#endif // STREAM9_LINUX_EPOLL_NAMESPACE_HPP
