#include <stream9/linux/epoll/epoll.hpp>

#include <stream9/linux/epoll/namespace.hpp>

#include <string>
#include <string_view>

#include <boost/container/small_vector.hpp>

#include <sys/epoll.h>

#include <stream9/strings/join.hpp>

namespace stream9::linux {

std::string_view
epoll_op_to_symbol(int const op) noexcept
{
    switch (op) {
        case EPOLL_CTL_ADD:
            return "EPOLL_CTL_ADD";
        case EPOLL_CTL_MOD:
            return "EPOLL_CTL_MOD";
        case EPOLL_CTL_DEL:
            return "EPOLL_CTL_DEL";
    }

    return "unknown";
}

std::string
epoll_events_to_symbol(std::uint32_t const events)
{
    boost::container::small_vector<std::string_view, 5> v;

    if (events & EPOLLIN) {
        v.push_back("EPOLLIN");
    }
    if (events & EPOLLOUT) {
        v.push_back("EPOLLOUT");
    }
    if (events & EPOLLRDHUP) {
        v.push_back("EPOLLRDHUP");
    }
    if (events & EPOLLPRI) {
        v.push_back("EPOLLPRI");
    }
    if (events & EPOLLERR) {
        v.push_back("EPOLLERR");
    }
    if (events & EPOLLHUP) {
        v.push_back("EPOLLHUP");
    }
    if (events & EPOLLET) {
        v.push_back("EPOLLET");
    }
    if (events & EPOLLONESHOT) {
        v.push_back("EPOLLONESHOT");
    }
    if (events & EPOLLWAKEUP) {
        v.push_back("EPOLLWAKEUP");
    }
    if (events & EPOLLEXCLUSIVE) {
        v.push_back("EPOLLEXCLUSIVE");
    }

    return str::join(v, " | ");
}

} // namespace stream9::linux
